const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  console.log('Request at ' + (new Date()))
  res.send("Hello World!\n")
})

app.listen(port, () => {
  console.log(`Example app listening at port ${port}`)
})
